# Purpose

This docker image covers the following:

- `chocolatey` package manager
- `python:3.11` interpreter including the `pip` package manager
- `gcc` and `gfortran` compilers
- `cmd` (default), `powershell`, or `bash` shells

# Usage

The entrypoint is the default command line in windows `cmd`. With either `- bash` `- powershell` you can switch to your desired shell and work from there.

